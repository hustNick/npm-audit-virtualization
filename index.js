const fs = require('fs');
const mysql = require('mysql2');
const path = require('path');
const { argv } = require('yargs');
const { exec } = require('child_process');

const dbCfg = require('./db.config.json');

const db = mysql.createConnection(dbCfg).promise();

const LEVELS = ['info', 'low', 'moderate', 'high', 'critical'];

const promisedExec = (command, options = {}) =>
  new Promise(resolve => {
    exec(
      command,
      {
        stdio: 'inherit',
        shell: true,
        ...options
      },
      (err, stdout) => {
        resolve(stdout);
      }
    );
  });

(async () => {
  const tmpFile = 'report.json';
  const { dir: cwd } = argv;

  await promisedExec(`npm audit --json > ${tmpFile}`, {
    cwd
  });

  const reportPath = path.join(cwd, tmpFile);
  const json = JSON.parse(fs.readFileSync(reportPath));
  fs.unlinkSync(reportPath);

  const gitHash = (await promisedExec('git rev-parse --short HEAD')).trim(
    /\n\r/g
  );

  const { metadata } = json;

  const auditItem = {
    gitHash,
    totalDependencies: metadata.totalDependencies,
    ...metadata.vulnerabilities
  };

  await db.beginTransaction();

  let auditId;

  try {
    const r = await db.query('INSERT INTO npm_audit SET ?', {
      git_hash: auditItem.gitHash,
      level_info: auditItem.info,
      level_low: auditItem.low,
      level_moderate: auditItem.moderate,
      level_high: auditItem.high,
      level_critical: auditItem.critical,
      total_dependencies: auditItem.totalDependencies
    });
    auditId = r[0].insertId;
  } catch (err) {
    await db.rollback();
    throw err;
  }

  const advisories = Object.values(json.advisories);

  for (let i = 0; i < advisories.length; i++) {
    const advisory = advisories[i];
    const { findings } = advisory;

    for (let j = 0; j < findings.length; j++) {
      const finding = findings[j];
      const { paths } = finding;

      for (let k = 0; k < paths.length; k++) {
        const path = paths[k];

        const vulnerabilityItem = {
          path,
          auditId,
          package: advisory.module_name,
          version: finding.version,
          dependencyOf: path.split('>').shift(),
          level: advisory.severity,
          advisoryId: advisory.id
        };

        const data = {
          package: vulnerabilityItem.package,
          path: vulnerabilityItem.path,
          package_version: vulnerabilityItem.version,
          dependency_of: vulnerabilityItem.dependencyOf,
          npm_audit_id: vulnerabilityItem.auditId,
          level: LEVELS.indexOf(vulnerabilityItem.level),
          advisory_id: vulnerabilityItem.advisoryId
        };

        try {
          await db.query('INSERT INTO vulnerability SET ?', data);
        } catch (err) {
          console.log(data);
          await db.rollback();
          throw err;
        }
      }
    }
  }

  await db.commit();
  db.destroy();
})();
